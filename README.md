# Statbot

Statbot is a web server which exposes a single JSON object on its root. It wraps
a Tinc process, pulling node and edge information periodically and exposing it
as a JSON object to clients.

## Example Output

```
{
  "nodes": [
    {
      "identifier": "UCTM001HOME",
      "address": "172.16.0.1",
      "port": 655,
      "owned_subnets": [
        "172.24.4.0/24"
      ]
    },
    {
      "identifier": "UCTM001TEST",
      "address": "MYSELF",
      "port": -1,
      "owned_subnets": [
        "172.24.6.1/32"
      ]
    }
  ],
  "edges": [
    {
      "from": "UCTM001HOME",
      "to": "UCTM001TEST",
      "weight": 215
    },
    {
      "from": "UCTM001TEST",
      "to": "UCTM001HOME",
      "weight": 215
    }
  ]
}
```

## Building

You can build statbot by running the following commands:

```
dep ensure
go build .
```

The executable will be named `statbot`.

## Running

Statbot will only run on Linux and FreeBSD systems due to the dependence on
\*NIX process signals. This is fine because Tinc also only runs on \*NIX based
systems.

Statbot has the following flags:

+ `-bind` - `-bind :8080` : Specifies which port the web server should bind to

Statbot takes the remaining non-flag arguments as the process which Statbot
should encapsulate.

Ex:

```
# ./statbot /usr/bin/tincd -n UCTINCNET -D
```

This will start statbot on its default port (`8080`) and expose information
about the UCTINCNET Tinc network.

# License

This work is licensed under the MIT license. This license can be found in the
LICENSE file.
