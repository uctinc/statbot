package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"regexp"
	"strconv"
	"syscall"
	"time"
)

type Signaler interface {
	Signal(s os.Signal) error
}

type chanpair struct {
	from, to chan []byte
}

type liner func([]byte) bool

func (c *chanpair) run(l liner) {
	for {
		m, more := <-c.from
		if !more {
			close(c.to)
			return
		}
		more = l(m)

		c.to <- m
		if !more {
			return
		}
	}
}

func (c *chanpair) drainChan() {
	for {
		select {
		case m, more := <-c.from:
			if !more {
				close(c.to)
				return
			}
			c.to <- m
		default:
			return
		}
	}
}

func (c *chanpair) scanThrough(prefix []byte) {
	c.run(func(b []byte) bool {
		return !bytes.HasPrefix(b, prefix)
	})
}

func (c *chanpair) nextLine() (r *[]byte) {
	r = nil
	c.run(func(b []byte) bool {
		r = &b
		return false
	})
	return
}

var nodeRegex = regexp.MustCompile("(\\w+) at ([\\w.]+) (?:port ([0-9]+))?")
var edgeRegex = regexp.MustCompile("(\\w+) to (\\w+) .* weight ([0-9]+)")
var subnetRegex = regexp.MustCompile("([0-9.]+\\/[0-9]{1,2})#[0-9]+ owner (\\w+)")

func scannerRip(r io.Reader, c chan []byte) {
	s := bufio.NewScanner(r)
	go func() {
		for s.Scan() {
			c <- s.Bytes()
		}
		close(c)
	}()
}

func wrapTinc(s Signaler, stdout io.Reader, stderr io.Reader) (chan []byte, chan *Response) {
	p := &chanpair{
		from: make(chan []byte),
		to:   make(chan []byte),
	}

	c1 := make(chan []byte)
	c2 := make(chan []byte)

	scannerRip(stdout, c1)
	scannerRip(stderr, c2)

	go func() {
		c1more := true
		c2more := true
		for c1more && c2more {
			select {
			case m, c1more := <-c1:
				if c1more {
					p.from <- m
				}
			case m, c2more := <-c2:
				if c2more {
					p.from <- m
				}
			}
		}

		if c1more {
			for c1more {
				m, c1more := <-c1
				if c1more {
					p.from <- m
				}
			}
		} else if c2more {
			for c2more {
				m, c2more := <-c2
				if c2more {
					p.from <- m
				}
			}
		}
		close(p.from)
	}()

	responses := make(chan *Response)

	go func() {
		defer close(responses)
		for {
			t := time.After(10 * time.Second)
		wait:
			for {
				select {
				case <-t:
					break wait
				case b := <-p.from:
					p.to <- b
				}
			}
			// send the signal to our signaler
			err := s.Signal(syscall.SIGUSR2) // very linux specific
			if err != nil {
				panic(err)
			}

			response := Response{
				Nodes: make([]Node, 0, 10),
				Edges: make([]Edge, 0, 10),
			}

			// queue will now be filled with our current info dump.
			// skip ahead to "Nodes:"
			p.scanThrough([]byte("Nodes:"))
			for {
				// we are through! scan and digest each line
				d := p.nextLine()
				if d == nil {
					return
				}
				submatches := nodeRegex.FindSubmatch(*d)
				if submatches == nil {
					break
				}

				var port int
				if string(submatches[2]) != "MYSELF" {
					port, err = strconv.Atoi(string(submatches[3]))
					if err != nil {
						continue // idk, something went wrong? shouldn't happen
					}
				} else {
					port = -1
				}

				response.Nodes = append(response.Nodes, Node{
					Identifier:   string(submatches[1]),
					Address:      string(submatches[2]),
					Port:         port,
					OwnedSubnets: make([]string, 0, 10),
				})
			}

			p.scanThrough([]byte("Edges:"))

			for {
				// we are through! scan and digest each line
				d := p.nextLine()
				if d == nil {
					return
				}
				submatches := edgeRegex.FindSubmatch(*d)
				if submatches == nil {
					break
				}

				weight, err := strconv.Atoi(string(submatches[3]))
				if err != nil {
					continue
				}

				response.Edges = append(response.Edges, Edge{
					From:   string(submatches[1]),
					To:     string(submatches[2]),
					Weight: weight,
				})
			}

			p.scanThrough([]byte("Subnet list:"))

			for {
				// we are through! scan and digest each line
				d := p.nextLine()
				if d == nil {
					return
				}

				matches := subnetRegex.FindSubmatch(*d)
				if matches == nil {
					break
				}

				owner := string(matches[2])
				for i := 0; i < len(response.Nodes); i++ {
					node := &response.Nodes[i]
					if node.Identifier == owner {
						node.OwnedSubnets = append(node.OwnedSubnets, string(matches[1]))
						break
					}
				}
			}
			responses <- &response
		}
	}()

	return p.to, responses
}
