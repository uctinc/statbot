package main

import (
	"flag"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	flag.StringVar(&bindAddr, "bind", ":8080", "-bind <address>")

	flag.Parse()

	e := echo.New()
	e.Debug = true
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Skipper: middleware.DefaultSkipper,
		Format:  "${time_rfc3339} - ${remote_ip} ${method} ${uri} ${status} ${error} ${latency_human}\n",
		Output:  os.Stdout,
	}))
	e.Use(middleware.Recover())

	subexe := flag.Args()
	if len(subexe) == 0 {
		log.Fatalf("Fatal: No subcommand passed!")
	}

	stdout, stdoutW, err := os.Pipe()
	if err != nil {
		log.Fatal(err)
	}
	stderr, stderrW, err := os.Pipe()
	if err != nil {
		log.Fatal(err)
	}

	attr := os.ProcAttr{
		Dir: "",
		Env: nil,
		Files: []*os.File{
			nil,     //stdin
			stdoutW, //stdout
			stderrW, //stderr
		},
		Sys: nil,
	}

	log.Printf("Starting %s %s", subexe[0], subexe[1:])
	proc, err := os.StartProcess(subexe[0], subexe, &attr)
	if err != nil {
		log.Fatal(err)
	}

	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGKILL)
	go func() {
		<-sigs
		log.Println("Terminating process...")
		proc.Signal(syscall.SIGQUIT)
	}()

	go func() {
		s, err := proc.Wait()
		if err != nil {
			panic(err)
		}
		log.Printf("Exited. %s", s)
		os.Exit(1)
	}()

	logs, responses := wrapTinc(proc, stdout, stderr)

	go func() {
		for {
			log.Printf("tinc> %s", string(<-logs))
		}
	}()

	serve(e, responses)
}
