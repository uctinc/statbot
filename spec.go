package main

type Node struct {
	Identifier   string   `json:"identifier"`
	Address      string   `json:"address"`
	Port         int      `json:"port"`
	OwnedSubnets []string `json:"owned_subnets"`
}

type Edge struct {
	From   string `json:"from"`
	To     string `json:"to"`
	Weight int    `json:"weight"`
}

type Response struct {
	Nodes []Node `json:"nodes"`
	Edges []Edge `json:"edges"`
}
