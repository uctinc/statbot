package main

import (
	"encoding/json"
	"github.com/labstack/echo"
	"net/http"
)

var bindAddr string

func serve(e *echo.Echo, responses chan *Response) {
	response := []byte("{}")

	go func() {
		var err error
		for {
			r := <-responses
			response, err = json.Marshal(r)
			if err != nil {
				panic(err)
			}
		}
	}()

	e.GET("/", func(c echo.Context) error {
		c.Response().Header().Set("Access-Control-Allow-Origin", "*")
		return c.JSONBlob(http.StatusOK, response)
	})

	e.Logger.Fatal(e.Start(bindAddr))
}
